﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeSolutions.Solutions
{
    internal class TwoSum
    {
        public class Solution
        {
            public int[] TwoSum(int[] nums, int target)
            {
                if(nums.Length == 2)
                {
                    return new int[] { 0, 1 };
                }
                for(int i = 0; i < nums.Length; i++)
                {
                    for(int j = 0; j < nums.Length; j++)
                    {
                        if(nums[i] + nums[j] == target && i != j)
                        {
                            return new int[] { i, j };
                        }
                    }
                }
                return new int[] { -1, -1 };
            }
        }
    }
}
