﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeSolutions.Solutions
{
    internal class RomanToInt
    {
        public class Solution
        {
            Dictionary<string, int> Mixes = new Dictionary<string, int>()
            {
                { "IV", 4 },
                { "IX", 9 },
                { "XL", 40},
                { "XC", 90},
                { "CD", 400},
                { "CM", 900}
            };
            Dictionary<string, int> Pures = new Dictionary<string, int>()
            {
                {"I",1 },
                {"V", 5 },
                {"X", 10 },
                {"L", 50 },
                {"C", 100 },
                {"D", 500 },
                {"M", 1000 }
            };
            public int RomanToInt(string s)
            {
                int total = 0;
                while (s.Length > 0)
                {
                    if (s.Length > 1 && Mixes.Keys.Contains(s.Substring(0, 2)))
                    {
                        total += Mixes[s.Substring(0, 2)];
                        s = s.Remove(0, 2);
                    }
                    else
                    {
                        total += Pures[s.Substring(0, 1)];
                        s = s.Remove(0, 1);
                    }
                }
                return total;
            }
        }
    }
}
