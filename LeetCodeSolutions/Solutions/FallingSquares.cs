﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeetCodeSolutions.Solutions
{
    internal class FallingSquares
    {
        public class Solution
        {
            class Item
            {
                public int Index { get; set; }
                public int Position { get; set; }
                public int Span { get; set; }
                public int Height { get; set; }
            };
            public IList<int> FallingSquares(int[][] positions)
            {
                List<int> result = new List<int>();
                List<Item> rs = new List<Item> ();
                for(int i = 0; i < positions.Length; i++)
                {
                    var cur = positions[i];
                    var item = new Item { Index = i, Position = cur[0], Span = cur[0] + cur[1], Height = cur[1] };
                    var below = rs.OrderByDescending(e => e.Height).Where(e => 
                        (e.Position <= item.Position && e.Span > item.Position)
                        || (e.Position < item.Span && e.Span >= item.Span)
                        || (item.Position <= e.Position && item.Span >= e.Span)
                        || (item.Position >= e.Position && item.Span <= e.Span)
                    ).FirstOrDefault();
                    if (below != null)
                    {
                        item.Height += below.Height;
                    }
                    rs.Add(item);
                    result.Add(rs.OrderByDescending(e => e.Height).First().Height);
                }
                return result;
            }
        }
    }
}
